export interface Entidad {
    nombre: string;
    localidad: string;
    provincia: string;
    kg_cajas: number;
    num_cajas: number;
}