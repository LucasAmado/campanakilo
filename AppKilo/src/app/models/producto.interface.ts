export interface Producto {
    numIdentificacion: number;
    nombre: string;
    kg_totales: number;
}