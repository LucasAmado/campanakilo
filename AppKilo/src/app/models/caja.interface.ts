export interface Caja {
    id_producto: string;
    nombre_producto: string;
    id_entidad: string;
    nombre_entidad: string;
    kg: number;
    numIdentificacion: number;
}