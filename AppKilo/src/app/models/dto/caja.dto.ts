export class CajaDto{

    constructor(
        public id_producto: string,
        public nombre_producto: string,
        public id_entidad: string,
        public nombre_entidad: string,
        public kg: number,
        public numIdentificacion: number
    ){}

    transformarDto() {
        return { 
            id_producto: this.id_producto,
            nombre_producto: this.nombre_producto, 
            id_entidad: this.id_entidad,
            nombre_entidad: this.nombre_entidad,
            kg: this.kg,
            numIdentificacion: this.numIdentificacion
        };
    }
}