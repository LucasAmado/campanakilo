export class ProductoDto {

    constructor(
        public numIdentificacion: number,
        public nombre: string,
        public kg_totales: number
    ) { }

    transformarDto() {
        return {
            numIdentificacion: this.numIdentificacion,
            nombre: this.nombre,
            kg_totales: this.kg_totales
        };
    }
}
