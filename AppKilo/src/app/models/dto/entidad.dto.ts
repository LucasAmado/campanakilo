export class EntidadDto{

    constructor(
        public nombre: string,
        public localidad: string,
        public provincia: string,
        public kg_cajas: number,
        public num_cajas: number,
    ){}

    transformarDto() {
        return { 
            nombre: this.nombre, 
            localidad: this.localidad,
            provincia: this.provincia,
            kg_cajas: this.kg_cajas,
            num_cajas: this.num_cajas,
        };
    }
}