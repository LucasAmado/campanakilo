import { Usuario } from './../models/user.interface';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UsuarioDto } from '../models/dto/user.dto';


const collectionName = 'users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private db: AngularFirestore) { }

  createUser(uid: string, userDto: UsuarioDto) {
    return this.db.collection(collectionName).doc(uid).set(
      userDto.transformarDto()
    );
  }

  getUser(uid: string): Observable<Usuario> {
    return this.db.collection(collectionName).doc<Usuario>(uid).valueChanges();
  }

  /*
  createUserLocal() {
    let uid = localStorage.getItem('uid');
    this.db.collection(collectionName).doc(uid).set(
      {
        nombre: localStorage.getItem('nombre'),
        email: localStorage.getItem('email'),
        photo: localStorage.getItem('photo')
      }
    );
  }
  */
}
