import { CajasService } from './cajas.service';
import { FirestoreResponse } from './../models/firestore-response.interface';
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { EntidadDto } from './../models/dto/entidad.dto';
import { Entidad } from '../models/entidad.interface';
import { Caja } from '../models/caja.interface';

const collectionName = 'entidades';

@Injectable({
  providedIn: 'root'
})
export class EntidadesService {

  constructor(
    private db: AngularFirestore) { }

  public createEntidad(entidadNueva: EntidadDto): Promise<DocumentReference> {
    const coleccionEntidades = this.db.collection<Entidad>(collectionName);
    return coleccionEntidades.add(entidadNueva.transformarDto());
  }

  public getEntidades() {
    return this.db.collection<Entidad>(collectionName, ref => ref.orderBy('nombre')).snapshotChanges();
  }

  public getEntidadesOrderByProvincia() {
    return this.db.collection<Entidad>(collectionName, ref => ref.orderBy('provincia')).snapshotChanges();
  }

  public getEntidadesOrderByLocalidad() {
    return this.db.collection<Entidad>(collectionName, ref => ref.orderBy('localidad')).snapshotChanges();
  }

  public getEntidadesOrderByNumCajas() {
    return this.db.collection<Entidad>(collectionName, ref => ref.orderBy('num_cajas')).snapshotChanges();
  }

  public getEntidadesOrderByKg() {
    return this.db.collection<Entidad>(collectionName, ref => ref.orderBy('kg_cajas')).snapshotChanges();
  }

  public getEntidad(id: string) {
    return this.db.collection(collectionName).doc<Entidad>(id).valueChanges();
  }

  public updateEntidad(idEntidad: string, dtoEntidad: EntidadDto) {
    return this.db.doc(`${collectionName}/${idEntidad}`).update(dtoEntidad.transformarDto());
  }

  public deleteEntidad(idEntidad: string) {
    return this.db.doc(`${collectionName}/${idEntidad}`).delete();
  }

}
