import { Producto } from './../models/producto.interface';
import { ProductoDto } from './../models/dto/producto.dto';
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';

const collectionName = 'productos';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private db: AngularFirestore) { }

  public createProducto(productoNuevo: ProductoDto): Promise<DocumentReference> {
    const coleccionProductos = this.db.collection<Producto>(collectionName);
    return coleccionProductos.add(productoNuevo.transformarDto());
  }

  public getProductos() {
    return this.db.collection<Producto>(collectionName, ref => ref.orderBy('nombre')).snapshotChanges();
  }

  public getProductosOrderByNumIdentificacion() {
    return this.db.collection<Producto>(collectionName, ref => ref.orderBy('numIdentificacion')).snapshotChanges();
  }

  public getProductosOrderByKg() {
    return this.db.collection<Producto>(collectionName, ref => ref.orderBy('kg_totales')).snapshotChanges();
  }

  public getProducto(id: string) {
    return this.db.collection(collectionName).doc<Producto>(id).valueChanges();
  }

  public updateProducto(idProducto: string, dtoProducto: ProductoDto) {
    return this.db.doc(`${collectionName}/${idProducto}`).update(dtoProducto.transformarDto());
  }

  public deleteProducto(idProducto: string) {

    return this.db.doc(`${collectionName}/${idProducto}`).delete();
  }
}
