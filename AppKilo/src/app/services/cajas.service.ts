import { ProductosService } from 'src/app/services/productos.service';
import { CajaDto } from './../models/dto/caja.dto';
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Caja } from '../models/caja.interface';
import { EntidadesService } from './entidades.service';
import { Producto } from '../models/producto.interface';

const collectionName = 'cajas'

@Injectable({
  providedIn: 'root'
})
export class CajasService {

  constructor(
    private db: AngularFirestore,
    private productosService: ProductosService,
    private entidadesService: EntidadesService
    ) { }

  public createCaja(cajaNueva: CajaDto): Promise<DocumentReference> {
    const coleccionCajas = this.db.collection<Caja>(collectionName);
    return coleccionCajas.add(cajaNueva.transformarDto());
  }

  public getCajas() {
    return this.db.collection<Caja>(collectionName, ref => ref.orderBy('nombre_producto')).snapshotChanges();
  }

  public getCajasOrderByEntidad() {
    return this.db.collection<Caja>(collectionName, ref => ref.orderBy('nombre_entidad')).snapshotChanges();
  }

  public getCajasOrderByKgDesc() {
    return this.db.collection<Caja>(collectionName, ref => ref.orderBy('kg', 'desc')).snapshotChanges();
  }

  public getCaja(id: string) {
    return this.db.collection(collectionName).doc<Caja>(id).valueChanges();
  }

  public updateCaja(idCaja: string, dtoCaja: CajaDto) {
    return this.db.doc(`${collectionName}/${idCaja}`).update(dtoCaja.transformarDto());
  }

  public deleteCaja(idCaja: string) {
    return this.db.doc(`${collectionName}/${idCaja}`).delete();
  }

  public calcularKgTotalesByProducto(producto: Producto){
    return this.db.collection(collectionName, result => result.where('producto.kg_totales', '<', -1)).snapshotChanges();
  }
  
}
