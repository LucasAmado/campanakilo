import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class MenuService {

  constructor(public translate: TranslateService) {}

  getAll() {
    return [
      {
        link: '/',
        label: this.translate.instant('Informe Productos'),
        icon: 'home'
      },
      {
        link: '/entidades',
        label: this.translate.instant('Entidades'),
        icon: 'folder'
      },
      {
        link: '/productos',
        label: this.translate.instant('Productos'),
        icon: 'fastfood'
      },
      {
        link: '/cajas',
        label: this.translate.instant('Cajas'),
        icon: 'markunread_mailbox'
      },
      {
        link: '/informe-entidades',
        label: this.translate.instant('Informe Entidades'),
        icon: 'assignment'
      },
    ];
  }
}
