import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatChipsModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
} from '@angular/material';

import { ChartsModule } from 'ng2-charts';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { RandomstringComponent } from './randomstring/randomstring.component';
import { ProductosDialogComponent } from './productos-dialog/productos-dialog.component';
import { EntidadesListadoComponent } from './entidades-listado/entidades-listado.component';
import { EntidadesDialogComponent } from './entidades-dialog/entidades-dialog.component';
import { ProductosListadoComponent } from './productos-listado/productos-listado.component';
import { CajasListadoComponent } from './cajas-listado/cajas-listado.component';
import { CajasDialogComponent } from './cajas-dialog/cajas-dialog.component';
import {MatSelectModule} from '@angular/material/select';
import { EntidadesInformeListadoComponent } from './entidades-informe-listado/entidades-informe-listado.component';
import { ProductosInformeListadoComponent } from './productos-informe-listado/productos-informe-listado.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatRadioModule, MAT_RADIO_DEFAULT_OPTIONS} from '@angular/material/radio';
import { ProductosInformeGraficaComponent } from './productos-informe-grafica/productos-informe-grafica.component';
import { EntidadesInformeGraficaComponent } from './entidades-informe-grafica/entidades-informe-grafica.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatChipsModule,
    ChartsModule,
    FormsModule,
    NgxDatatableModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    MatSelectModule,
    MatPaginatorModule,
    MatRadioModule
  ],
  declarations: [DashboardComponent, RandomstringComponent, ProductosInformeListadoComponent, ProductosDialogComponent, EntidadesListadoComponent, EntidadesDialogComponent, ProductosListadoComponent, CajasListadoComponent, CajasDialogComponent, EntidadesInformeListadoComponent, ProductosInformeListadoComponent, ProductosInformeGraficaComponent, EntidadesInformeGraficaComponent],
  entryComponents: [ProductosDialogComponent, EntidadesDialogComponent, CajasDialogComponent],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}},
    AuthService,
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'warn'},
  }
  ]
})
export class DashboardModule {}
