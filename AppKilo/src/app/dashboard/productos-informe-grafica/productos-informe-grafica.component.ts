import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Producto } from 'src/app/models/producto.interface';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-productos-informe-grafica',
  templateUrl: './productos-informe-grafica.component.html',
  styleUrls: ['./productos-informe-grafica.component.scss']
})
export class ProductosInformeGraficaComponent implements OnInit {
  arrayKilos: number[] = [];
  listaProductos: FirestoreResponse<Producto>[];
  public barChartLabels: any[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public barChartData: ChartDataSets[] = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  constructor(private productosService: ProductosService) { }

  ngOnInit() {
    this.loadProductos();
  }

  loadProductos(){
    this.productosService.getProductosOrderByNumIdentificacion().subscribe(resp => {
      this.listaProductos = [];

      resp.forEach((producto: any) => {
        this.listaProductos.push({ 
          id: producto.payload.doc.id, 
          data: producto.payload.doc.data() as Producto 
        });
      });

      //Cargar datos de los productos
      this.listaProductos.forEach(element=>{
        this.barChartLabels.push(element.data.nombre)
        this.arrayKilos.push(element.data.kg_totales)
      });
    });

    this.barChartData.push(
      {data: this.arrayKilos, backgroundColor: '#4ecf57', label: 'Cantidad'}
    )
  }

}