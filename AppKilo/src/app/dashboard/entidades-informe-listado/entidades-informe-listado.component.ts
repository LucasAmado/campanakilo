import { Caja } from './../../models/caja.interface';
import { CajasService } from './../../services/cajas.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { Entidad } from 'src/app/models/entidad.interface';
import { EntidadesService } from 'src/app/services/entidades.service';
import { MatDialog, MatSnackBar, MatPaginator, MatTableDataSource } from '@angular/material';
import { EntidadesDialogComponent } from './../entidades-dialog/entidades-dialog.component';

@Component({
  selector: 'app-entidades-informe-listado',
  templateUrl: './entidades-informe-listado.component.html',
  styleUrls: ['./entidades-informe-listado.component.scss']
})
export class EntidadesInformeListadoComponent implements OnInit {
  listadoEntidades: FirestoreResponse<Entidad>[];
  listadoCajas: FirestoreResponse<Caja>[];
  displayedColumns: string[] = ['id', 'nombre', 'provincia', 'localidad', 'kg_cajas', 'num_cajas', 'edit', 'delete'];
  dataSource;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private entidadesService: EntidadesService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private cajasService: CajasService) {
  }

  ngOnInit() {
    this.loadEntidades();
    this.loadCajas();
  }

  loadCajas() {
    this.cajasService.getCajas().subscribe(resp => {
      this.listadoCajas = [];
      resp.forEach((caja: any) => {
        this.listadoCajas.push({
          id: caja.payload.doc.id,
          data: caja.payload.doc.data() as Caja
        });
      });
      //Actualizar el número de cajas por entidad
      this.listadoEntidades.forEach(element => {
        if (this.listadoCajas.forEach(element => {
          element.data.nombre_entidad
        }) == this.listadoEntidades.forEach(element => {
          element.data.nombre
        })) {
          element.data.num_cajas = this.listadoCajas.filter((obj) => obj.data.nombre_entidad === element.data.nombre).length;
        }
      });
    });
  }

  loadEntidades() {
    this.entidadesService.getEntidades().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
      });

      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;

    });
  }

  dialogNuevaEntidad() {
    let dialogRef = this.dialog.open(EntidadesDialogComponent, {
      width: '300px',
      data: { editar: false }
    });

    dialogRef.afterClosed().subscribe(resp => {
      if (resp != null) {
        if (resp) {
          this.snackBar.open("Entidad creada correctamente");
        } else {
          this.snackBar.open("Error al crear la Entidad");
        }
      }
    });
  }

  dialogEditarEntidad(idEntidad: string) {
    let dialogRef = this.dialog.open(EntidadesDialogComponent, {
      width: '300px',
      data: { idEntidadEditar: idEntidad, editar: true }
    });

    dialogRef.afterClosed().subscribe(resp => {
      if (resp != null) {
        if (resp) {
          this.snackBar.open("Entidad actualizada correctamente");
        } else {
          this.snackBar.open("Error al actualizar la entidad");
        }
      }
    });
  }

  eliminarEntidad(idEntidad: string) {
    if (window.confirm("¿Quiere borrar la entidad?")) {
      this.entidadesService.deleteEntidad(idEntidad);
    }
  }

  loadEntidadesByProvincia() {
    this.entidadesService.getEntidadesOrderByProvincia().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }

  loadEntidadesByLocalidad() {
    this.entidadesService.getEntidadesOrderByLocalidad().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }

  loadEntidadesByNumCajas() {
    this.entidadesService.getEntidadesOrderByNumCajas().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }

  loadEntidadesByKg() {
    this.entidadesService.getEntidadesOrderByKg().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
        this.listadoEntidades.forEach(element => {
          if (this.listadoCajas.forEach(element => {
            element.data.nombre_entidad
          }) == this.listadoEntidades.forEach(element => {
            element.data.nombre
          })) {
            element.data.num_cajas = this.listadoCajas.filter((obj) => obj.data.nombre_entidad === element.data.nombre).length;
          }
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }
}
