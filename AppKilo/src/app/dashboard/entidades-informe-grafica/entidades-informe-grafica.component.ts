import { CajasService } from './../../services/cajas.service';
import { FirestoreResponse } from './../../models/firestore-response.interface';
import { element } from 'protractor';
import { EntidadesService } from 'src/app/services/entidades.service';
import { Component, OnInit } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { Entidad } from 'src/app/models/entidad.interface';
import { Caja } from 'src/app/models/caja.interface';

@Component({
  selector: 'app-entidades-informe-grafica',
  templateUrl: './entidades-informe-grafica.component.html',
  styleUrls: ['./entidades-informe-grafica.component.scss']
})
export class EntidadesInformeGraficaComponent implements OnInit {
  arrayKilos: number[] = [];
  arrayNumCajas: number[] = [];
  listadoEntidades: FirestoreResponse<Entidad>[];
  listadoCajas: FirestoreResponse<Caja>[];
  public barChartLabels: any[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public barChartData: ChartDataSets[] = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  constructor(
    private entidadesService: EntidadesService,
    private cajasService: CajasService) { }

  ngOnInit() {
    this.loadCajas();
    this.loadEntidades();
  }

  loadCajas() {
    this.cajasService.getCajas().subscribe(resp => {
      this.listadoCajas = [];

      resp.forEach((caja: any) => {
        this.listadoCajas.push({
          id: caja.payload.doc.id,
          data: caja.payload.doc.data() as Caja
        });
      });
    });
  }


  loadEntidades() {
    this.entidadesService.getEntidades().subscribe(resp => {
      this.listadoEntidades = [];
      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
      });

      this.listadoEntidades.forEach(element => {
        if (this.listadoCajas.forEach(element => {
          element.data.nombre_entidad
        }) == this.listadoEntidades.forEach(element => {
          element.data.nombre
        })) {
          element.data.num_cajas = this.listadoCajas.filter((obj) => obj.data.nombre_entidad === element.data.nombre).length;
          //element.data.kg_cajas = this.listadoCajas.filter((obj) => obj.data.nombre_entidad === element.data.nombre).length;
        }
      });

      this.listadoEntidades.forEach(element => {
        this.barChartLabels.push(element.data.nombre);
        this.arrayNumCajas.push(element.data.num_cajas);
        this.arrayKilos.push(element.data.kg_cajas);
      });
    });

    this.barChartData.push(
      { data: this.arrayNumCajas, backgroundColor: '#d13b3b', label: 'Cajas' },
      { data: this.arrayKilos, backgroundColor: '#4ecf57', label: 'Kg' })
  }

}