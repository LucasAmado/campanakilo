import { CajasService } from 'src/app/services/cajas.service';
import { Caja } from 'src/app/models/caja.interface';
import { ProductosDialogComponent } from './../productos-dialog/productos-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Producto } from 'src/app/models/producto.interface';
import { ProductosService } from 'src/app/services/productos.service';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { MatDialog, MatSnackBar, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-productos-informe-listado',
  templateUrl: './productos-informe-listado.component.html',
  styleUrls: ['./productos-informe-listado.component.scss']
})
export class ProductosInformeListadoComponent implements OnInit {

  listadoProductos: FirestoreResponse<Producto>[];
  displayedColumns: string[] = ['id', 'num', 'nombre', 'kg', 'edit', 'delete'];
  listaCajas: FirestoreResponse<Caja>[];
  dataSource;
  
  @ViewChild (MatPaginator, { static : true }) paginator: MatPaginator;

  constructor(
    private productosService: ProductosService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadProductos();
  }

  loadProductos() {
    this.productosService.getProductos().subscribe(resp => {
      this.listadoProductos = [];

      resp.forEach((producto: any) => {
        this.listadoProductos.push({
          id: producto.payload.doc.id,
          data: producto.payload.doc.data() as Producto
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Producto>>(this.listadoProductos);
      this.dataSource.paginator = this.paginator;
    });
  }

  dialogNuevoProducto() {
    let dialogRef = this.dialog.open(ProductosDialogComponent, {
      width: '300px',
      data: { editar: false }
    });

    dialogRef.afterClosed().subscribe(resp => {
      if (resp != null) {
        if (resp) {
          this.snackBar.open("Producto creado correctamente");
        } else {
          this.snackBar.open("Error al crear el producto");
        }
      }
    });
  }

  dialogEditarProducto(idProducto: string) {
    let dialogRef = this.dialog.open(ProductosDialogComponent, {
      width: '300px',
      data: { idProductoEditar: idProducto, editar: true }
    });

    dialogRef.afterClosed().subscribe(resp => {
      if (resp != null) {
        if (resp) {
          this.snackBar.open("Producto actualizado correctamente");
        } else {
          this.snackBar.open("Error al actualizar el producto");
        }
      }
    });
  }

  loadProductosBynumIdentificacion(){
    this.productosService.getProductosOrderByNumIdentificacion().subscribe(resp => {
      this.listadoProductos = [];

      resp.forEach((producto: any) => {
        this.listadoProductos.push({ 
          id: producto.payload.doc.id, 
          data: producto.payload.doc.data() as Producto 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Producto>>(this.listadoProductos);
      this.dataSource.paginator = this.paginator;
    });
  }

  loadProductosByKgProductos(){
    this.productosService.getProductosOrderByKg().subscribe(resp => {
      this.listadoProductos = [];

      resp.forEach((producto: any) => {
        this.listadoProductos.push({ 
          id: producto.payload.doc.id, 
          data: producto.payload.doc.data() as Producto 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Producto>>(this.listadoProductos);
      this.dataSource.paginator = this.paginator;
    });
  }

  eliminarProducto(idProducto: string) {
    if (window.confirm("¿Quiere borrar el producto?")) { 
      this.productosService.deleteProducto(idProducto);
    }
  }
}
