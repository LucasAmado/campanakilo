import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { Entidad } from 'src/app/models/entidad.interface';
import { EntidadesService } from 'src/app/services/entidades.service';
import { MatDialog, MatSnackBar, MatTableDataSource, MatPaginator } from '@angular/material';
import { EntidadesDialogComponent } from './../entidades-dialog/entidades-dialog.component';

@Component({
  selector: 'app-entidades-listado',
  templateUrl: './entidades-listado.component.html',
  styleUrls: ['./entidades-listado.component.scss']
})
export class EntidadesListadoComponent implements OnInit {
  listadoEntidades: FirestoreResponse<Entidad>[];
  displayedColumns: string[] = ['id', 'nombre', 'provincia', 'localidad', 'edit', 'delete'];
  dataSource;
  
  @ViewChild (MatPaginator, { static : true }) paginator: MatPaginator;

  constructor(
    private entidadesService: EntidadesService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadEntidades();
  }

  loadEntidades() {
    this.entidadesService.getEntidades().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({ 
          id: entidad.payload.doc.id, 
          data: entidad.payload.doc.data() as Entidad 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }

  dialogNuevaEntidad() {
    let dialogRef = this.dialog.open(EntidadesDialogComponent, {
      width: '300px',
      data: {editar : false}
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Entidad creada correctamente", );
        } else {
          this.snackBar.open("Error al crear la Entidad");
        }
      }
    });
  }

  dialogEditarEntidad(idEntidad: string) {
    let dialogRef = this.dialog.open(EntidadesDialogComponent, {
      width: '300px',
      data: {idEntidadEditar: idEntidad, editar : true}
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Entidad actualizada correctamente");
        } else {
          this.snackBar.open("Error al actualizar la entidad");
        }
      }
    });
  }

  eliminarEntidad(idEntidad: string){
    if (window.confirm("¿Quiere borrar la entidad?")) { 
      this.entidadesService.deleteEntidad(idEntidad);
    }
  }

  loadEntidadesByProvincia() {
    this.entidadesService.getEntidadesOrderByProvincia().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({ 
          id: entidad.payload.doc.id, 
          data: entidad.payload.doc.data() as Entidad 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }

  loadEntidadesByLocalidad() {
    this.entidadesService.getEntidadesOrderByLocalidad().subscribe(resp => {
      this.listadoEntidades = [];

      resp.forEach((entidad: any) => {
        this.listadoEntidades.push({ 
          id: entidad.payload.doc.id, 
          data: entidad.payload.doc.data() as Entidad 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Entidad>>(this.listadoEntidades);
      this.dataSource.paginator = this.paginator;
    });
  }

}
