import { ProductosInformeGraficaComponent } from './productos-informe-grafica/productos-informe-grafica.component';
import { CajasListadoComponent } from './cajas-listado/cajas-listado.component';
import { Routes } from '@angular/router';
import { EntidadesListadoComponent } from './entidades-listado/entidades-listado.component';
import { ProductosListadoComponent } from './productos-listado/productos-listado.component';
import { ProductosInformeListadoComponent } from './productos-informe-listado/productos-informe-listado.component';
import { EntidadesInformeListadoComponent } from './entidades-informe-listado/entidades-informe-listado.component';
import { EntidadesInformeGraficaComponent } from './entidades-informe-grafica/entidades-informe-grafica.component';


export const DashboardRoutes: Routes = [
  { path: '', component: ProductosInformeListadoComponent},
  { path: 'informe-productos', component: ProductosInformeListadoComponent },
  { path: 'informe-productos/grafica', component: ProductosInformeGraficaComponent },
  { path: 'entidades', component: EntidadesListadoComponent },
  { path: 'productos', component: ProductosListadoComponent },
  { path: 'cajas', component: CajasListadoComponent },
  { path: 'informe-entidades', component: EntidadesInformeListadoComponent },
  { path: 'informe-entidades/grafica', component: EntidadesInformeGraficaComponent }
];
