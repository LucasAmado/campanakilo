import { Caja } from './../../models/caja.interface';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { CajasService } from 'src/app/services/cajas.service';
import { MatDialog, MatSnackBar, MatPaginator, MatTableDataSource } from '@angular/material';
import { CajasDialogComponent } from '../cajas-dialog/cajas-dialog.component';

@Component({
  selector: 'app-cajas-listado',
  templateUrl: './cajas-listado.component.html',
  styleUrls: ['./cajas-listado.component.scss']
})
export class CajasListadoComponent implements OnInit {
  listadoCajas: FirestoreResponse<Caja>[];
  displayedColumns: string[] = ['id', 'numIdentificacion', 'nomProducto', 'kg', 'nomEntidad', 'edit', 'delete'];
  dataSource;
  cajaKg: FirestoreResponse<Caja>;
  
  @ViewChild (MatPaginator, { static : true }) paginator: MatPaginator;

  constructor(
    private cajasService: CajasService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadCajas();
  }

  loadCajas() {
    this.cajasService.getCajasOrderByEntidad().subscribe(resp => {
      this.listadoCajas = [];

      resp.forEach((caja: any) => {
        this.listadoCajas.push({ 
          id: caja.payload.doc.id, 
          data: caja.payload.doc.data() as Caja 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Caja>>(this.listadoCajas);
      this.dataSource.paginator = this.paginator;
    });
  }

  dialogNuevaCaja() {
    let dialogRef = this.dialog.open(CajasDialogComponent, {
      width: '300px',
      data: {editar : false}
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Caja creada correctamente", );
        } else {
          this.snackBar.open("Error al crear la producta");
        }
      }
    });
  }

  dialogEditarCaja(idCaja: string) {
    let dialogRef = this.dialog.open(CajasDialogComponent, {
      width: '300px',
      data: {idCajaEditar: idCaja , editar : true}
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Caja actualizada correctamente");
        } else {
          this.snackBar.open("Error al actualizar la caja");
        }
      }
    });
  }

  eliminarCaja(idCaja: string){
    if (window.confirm("¿Quiere borrar la caja?")) { 
      this.cajasService.deleteCaja(idCaja);
    }
  }

  loadCajasOrderByProducto() {
    this.cajasService.getCajas().subscribe(resp => {
      this.listadoCajas = [];

      resp.forEach((caja: any) => {
        this.listadoCajas.push({ 
          id: caja.payload.doc.id, 
          data: caja.payload.doc.data() as Caja 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Caja>>(this.listadoCajas);
      this.dataSource.paginator = this.paginator;
    });
  }

  loadCajasOrderByKg() {
    this.cajasService.getCajasOrderByKgDesc().subscribe(resp => {
      this.listadoCajas = [];

      resp.forEach((caja: any) => {
        this.listadoCajas.push({ 
          id: caja.payload.doc.id, 
          data: caja.payload.doc.data() as Caja 
        });
      });
      this.dataSource = new MatTableDataSource<FirestoreResponse<Caja>>(this.listadoCajas);
      this.dataSource.paginator = this.paginator;
    });
  }

  calcularKgByProducto(idProducto: string){
    this.listadoCajas.forEach(element => {

    });
  }

}
