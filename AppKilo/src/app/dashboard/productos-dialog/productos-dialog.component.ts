import { Producto } from 'src/app/models/producto.interface';
import { ProductoDto } from './../../models/dto/producto.dto';
import { ProductosService } from 'src/app/services/productos.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface DatosEntradaDialog {
  idProductoEditar: string,
  editar: boolean
}

@Component({
  selector: 'app-productos-dialog',
  templateUrl: './productos-dialog.component.html',
  styleUrls: ['./productos-dialog.component.scss']
})
export class ProductosDialogComponent implements OnInit {
  productoDto: ProductoDto;
  productoEditar: Producto;

  constructor(
    public dialogRef: MatDialogRef<ProductosDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DatosEntradaDialog,
    private productosService: ProductosService

  ) { }

  ngOnInit() {
    if (this.data.editar == true) {
      this.productosService.getProducto(this.data.idProductoEditar).subscribe(resp => {
        this.productoEditar = resp;
        this.productoDto = new ProductoDto(this.productoEditar.numIdentificacion, this.productoEditar.nombre, this.productoEditar.kg_totales);
      });

    } else {
      //nº identificación, nombre, kg_totales
      this.productoDto = new ProductoDto(1, '', 0);
    }
  }

  guardarProducto() {
    const editar = this.data.editar;
    if (editar == true) {
      const id = this.data.idProductoEditar;
      this.productosService.updateProducto(id, this.productoDto).then(respCorrecta => {
        this.dialogRef.close(true);
      }).catch(respError => {
        this.dialogRef.close(false);
      })
    } else {
      this.productosService.createProducto(this.productoDto).then(respCorrecta => {
        this.dialogRef.close(true);
      }).catch(respError => {
        this.dialogRef.close(false);
      })
    }

  }

  cerrar() {
    this.dialogRef.close(null);
  }

}
