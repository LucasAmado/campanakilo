import { EntidadesService } from 'src/app/services/entidades.service';
import { Entidad } from 'src/app/models/entidad.interface';
import { ProductosService } from 'src/app/services/productos.service';
import { Producto } from 'src/app/models/producto.interface';
import { Component, OnInit, Inject } from '@angular/core';
import { CajaDto } from 'src/app/models/dto/caja.dto';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CajasService } from 'src/app/services/cajas.service';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { Caja } from 'src/app/models/caja.interface';

export interface DatosEntradaDialog {
  idCajaEditar: string,
  editar: boolean
}

@Component({
  selector: 'app-cajas-dialog',
  templateUrl: './cajas-dialog.component.html',
  styleUrls: ['./cajas-dialog.component.scss']
})

export class CajasDialogComponent implements OnInit {
  cajaDto: CajaDto;
  cajaEditar: Caja;
  entidadSeleccionada: FirestoreResponse<Entidad>;
  productoSeleccionado: FirestoreResponse<Producto>;
  listaProductos: FirestoreResponse<Producto>[];
  listaEntidades: FirestoreResponse<Entidad>[];

  constructor(
    public dialogRef: MatDialogRef<CajasDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DatosEntradaDialog,
    private cajasService: CajasService,
    private productosService: ProductosService,
    private entidadesService: EntidadesService
  ) { }


  ngOnInit() {
    this.cargarProductos();
    this.cargarEntidades();

    if (this.data.editar == true) {
      this.cajasService.getCaja(this.data.idCajaEditar).subscribe(resp => {
        this.cajaEditar = resp;
        this.cajaDto = new CajaDto(this.cajaEditar.id_producto, this.cajaEditar.nombre_producto, this.cajaEditar.id_entidad, this.cajaEditar.nombre_entidad, this.cajaEditar.kg, this.cajaEditar.numIdentificacion);
      });
    } else {
      //id_producto, nom_producto, id_entidad, nom_entidad, kg, num
      this.cajaDto = new CajaDto('', '', '', 'Sin asignar', 20, 1);
    }
  }

  guardarCaja() {
    //Modificar datos producto
    //Se hace siempre al guardar la caja. De lo contrario los datos del producto no se guardan correctamente
    this.cajaDto.id_producto = this.productoSeleccionado.id;
    this.cajaDto.nombre_producto = this.productoSeleccionado.data.nombre;

    if (this.data.editar == true) {
    //Modificar datos entidad
    //Se hace solo al editar. Así al crear la caja no está asignada a ninguna entidad
    this.cajaDto.id_entidad = this.entidadSeleccionada.id;
    this.cajaDto.nombre_entidad = this.entidadSeleccionada.data.nombre;

      const id = this.data.idCajaEditar;
      this.cajasService.updateCaja(id, this.cajaDto).then(respCorrecta => {
        this.dialogRef.close(true);
      }).catch(respError => {
        this.dialogRef.close(false);
      })

    } else {
      this.cajasService.createCaja(this.cajaDto).then(respCorrecta => {
        this.dialogRef.close(true);
      }).catch(respError => {
        this.dialogRef.close(false);
      })
    }

  }

  cerrar() {
    this.dialogRef.close(null);
  }

  cargarProductos() {
    this.productosService.getProductos().subscribe(resp => {
      this.listaProductos = [];

      resp.forEach((producto: any) => {
        this.listaProductos.push({
          id: producto.payload.doc.id,
          data: producto.payload.doc.data() as Producto
        });
      });
      //Para que el formulario no explote al crear una un producto se muestra uno por defecto
      this.productoSeleccionado = this.listaProductos[0];

    });
  }

  cargarEntidades() {
    this.entidadesService.getEntidades().subscribe(resp => {
      this.listaEntidades = [];

      resp.forEach((entidad: any) => {
        this.listaEntidades.push({
          id: entidad.payload.doc.id,
          data: entidad.payload.doc.data() as Entidad
        });
      });

      //Para que el formulario no explote al crear una nueva entidad (nula) se pone que coja una entidad por defecto
      this.entidadSeleccionada = this.listaEntidades[0];

    });
  }

}
