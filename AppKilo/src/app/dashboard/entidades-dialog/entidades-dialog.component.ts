import { Component, OnInit, Inject } from '@angular/core';
import { EntidadDto } from 'src/app/models/dto/entidad.dto';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EntidadesService } from 'src/app/services/entidades.service';
import { Entidad } from 'src/app/models/entidad.interface';

export interface DatosEntradaDialog {
  idEntidadEditar: string,
  editar: boolean
}

@Component({
  selector: 'app-entidades-dialog',
  templateUrl: './entidades-dialog.component.html',
  styleUrls: ['./entidades-dialog.component.scss']
})
export class EntidadesDialogComponent implements OnInit {
  entidadDto: EntidadDto;
  entidadEditar: Entidad;

  constructor(
    public dialogRef: MatDialogRef<EntidadesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DatosEntradaDialog,
    private entidadesService: EntidadesService,
  ) {}

  ngOnInit() {
    if (this.data.editar == true) {
      this.entidadesService.getEntidad(this.data.idEntidadEditar).subscribe(resp => {
        this.entidadEditar = resp;
        this.entidadDto = new EntidadDto(this.entidadEditar.nombre, this.entidadEditar.localidad, this.entidadEditar.provincia, this.entidadEditar.kg_cajas, this.entidadEditar.num_cajas);
      });

    } else {
      this.entidadDto = new EntidadDto('', '', '', 0, 0);
    }
  }

  guardarEntidad() {
    const editar = this.data.editar;
    if (editar == true) {
      const id = this.data.idEntidadEditar;
      this.entidadesService.updateEntidad(id, this.entidadDto).then(respCorrecta => {
        this.dialogRef.close(true);
      }).catch(respError => {
        this.dialogRef.close(false);
      })
    } else {
      this.entidadesService.createEntidad(this.entidadDto).then(respCorrecta => {
        this.dialogRef.close(true);
      }).catch(respError => {
        this.dialogRef.close(false);
      })
    }

  }

  cerrar() {
    this.dialogRef.close(null);
  }

}
