// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDy6ylwa0uXTq4jQyOEPPRd8Awq2kgXu8s",
    authDomain: "campanakilo.firebaseapp.com",
    databaseURL: "https://campanakilo.firebaseio.com",
    projectId: "campanakilo",
    storageBucket: "campanakilo.appspot.com",
    messagingSenderId: "210649344501",
    appId: "1:210649344501:web:a1f2993fb9b75f7525d8bc"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
